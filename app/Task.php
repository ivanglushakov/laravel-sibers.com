<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class Task extends Model
{
    protected $fillable = [
        'name',
        'status',
        'description',
        'category_id',
        'user_id',
        'assigment_user_id'
    ];

    /**
     * @return array
     */
    public static function rulesCreate()
    {
        return [
            'name' => 'required|max:255|unique:tasks',
            'status' => 'required',
            'description' => 'required',
            'category_id' => 'required',
            'photo' => 'required',
            'photo.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public static function messages()
    {
        return [
            'required' => 'Error!, a :attribute is required',
        ];
    }

    /**
     * Get category task
     *
     */
    public function category()
    {
        return $this->belongsTo(TaskCategory::class, 'category_id', 'id');
    }

    /**
     * The tasks that belong to the photo.
     */
    public function photo()
    {
        return $this->belongsToMany(Photo::class, 'tasks_photos');
    }
}
