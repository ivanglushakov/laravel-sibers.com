<?php

namespace App\Console\Commands;

use App\Role;
use App\User;
use Illuminate\Console\Command;

class AddRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add:role {email} {role}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add role user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->argument('email');
        $role  = $this->argument('role');

        $userModel = User::where(['email' => $email])->first();
        $roleModel = Role::where(['name' => $role])->first();

        if (!$userModel instanceof User || !$roleModel instanceof Role) {
            $this->info("User not found or Role not Found");
            return false;
        }

        if ($userModel->hasRole($role)) {
            $this->error("Role already exists");
            return false;
        }

        $userModel->roles()->attach($roleModel->id);
        $userModel->save();

        $this->info("User = $email, Role = $role");
        $this->info("Role has been add");
    }
}
