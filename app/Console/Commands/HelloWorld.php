<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class HelloWorld extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hello:world {world?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command say worlds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $world = $this->ask('What is your world?');

//        if ($this->confirm('Do you wish to continue?')) {
//            //
//        }
        $name = $this->argument('world');

        $this->info($name);
    }
}
