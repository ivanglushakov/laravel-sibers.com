<?php

namespace App\Console;

use App\Console\Commands\AddRole;
use App\Console\Commands\HelloWorld;
use App\Helpers\Generate;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        HelloWorld::class,
        AddRole::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule = $schedule->command(HelloWorld::class, [Generate::randomString()])
            ->cron('* * * * * *');

        if (env('MAIL_SEND')) {
            $schedule->emailOutputTo(env('MAIL_USERNAME'));
        }

        $schedule->withoutOverlapping();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
