<?php

namespace App\Mail;

use App\Task;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailWhenTaskCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Task
     */
    private $task;

    /**
     * Create a new message instance.
     *
     * @param Task $task
     * @return void
     */
    public function __construct(Task $task)
    {
        $this->task = $task;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id       = $this->task->id;
        $nameTask = $this->task->name;

        return $this->subject("Created Task #$id \"$nameTask\" ")
            ->markdown('mail.task.create', [
                'task' => $this->task
            ]);
    }
}
