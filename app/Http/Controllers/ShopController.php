<?php

namespace App\Http\Controllers;

use App\Product;
use App\Services\Card;
use App\Services\CardItem;
use Illuminate\Http\Request;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class ShopController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function productList(Request $request)
    {
        // If card not exist in session
        if (!$request->session()->exists(Card::SESSION_CARD)) {
            $card = new Card();
            $request->session()->put(Card::SESSION_CARD, $card);
        } else {
            $card = $request->session()->get(Card::SESSION_CARD);
        }

        return view('shop.list', [
            'products' => Product::enable()->get(),
            'count'    => $card->getCountProduct(),
            'total'    => $card->getTotal(),
        ]);
    }

    /**
     * Add product in card
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addProductInCard(Request $request)
    {
        if (!$request->isXmlHttpRequest()){
            return response()->json([
                'status' => false,
                'message' => 'Bad request'
            ], 400);
        }

        $product = Product::where('id', $request->product)->firstOrFail();

        /**@var Card $card*/
        $card = $request->session()->get(Card::SESSION_CARD);

        $cardItem = new CardItem($product);

        $card->addProduct($cardItem);

        $request->session()->put(Card::SESSION_CARD, $card);

        return response()->json([
            'status'  => true,
            'message' => 'Product has been added!',
            'count'   => $card->getCountProduct(),
            'total'   => $card->getTotal(),
        ], 200);
    }
}
