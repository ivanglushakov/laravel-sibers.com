<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Services\Card;
use Illuminate\Http\Request;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class CardController extends Controller
{
    public function index(Request $request)
    {
        /**@var Card $card*/
        $card = $request->session()->get(Card::SESSION_CARD);

        if ($card instanceof Card) {
            return view('card.index', [
                'items' => $card->getItems(),
                'card'  => $card,
                'count' => $card->getCountProduct(),
                'total' => $card->getTotal(),
            ]);
        }

        return view('card.index', [
            'items' => '',
            'card'  => '',
            'count' => 0,
            'total' => 0,
        ]);

    }
}
