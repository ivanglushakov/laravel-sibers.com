<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\StripePayment;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class PayController extends Controller
{
    /**
     * @var StripePayment
     */
    private $stripePayment;

    /**
     * @param StripePayment $stripePayment
     */
    public function __construct(StripePayment $stripePayment)
    {
        $this->stripePayment = $stripePayment;
    }

    public function payment()
    {
        return view('admin.pay.payment');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function createCharge(Request $request):RedirectResponse
    {
        $isCharge = $this->stripePayment->createCharge($request, \Auth::user()->email);

        if (!$isCharge) {
            return back()
                ->with('error', 'Payment error!');
        }

        return back()
            ->with('success', 'Payment success!');
    }
}
