<?php

namespace App\Http\Controllers\Admin;

use App\Services\UploadImageService;
use App\Photo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class PhotoController extends Controller
{
    /**
     * @var UploadImageService
     */
    private $imageService;

    /**
     * @param UploadImageService $imageService
     */
    public function __construct(UploadImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $photos = Photo::all();
        return view('admin.photo.index', compact('photos'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function uploadImage(Request $request):RedirectResponse
    {
        $request->validate([
            'image' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $isUpload = $this->imageService->uploadImage($request, 'image');

        if (!is_array($isUpload) && !$isUpload) {
            return redirect()
                ->route('admin_photo')->with('error', 'Error!');
        }

        return redirect()
            ->route('admin_photo')->with('success', 'Images has been uploaded!');
    }

    /**
     * @param Photo $photo
     * @return RedirectResponse
     */
    public function destroy(Photo $photo):RedirectResponse
    {
        try {
            $photo->delete();

            $isDeleteImageToServer = $this->imageService->destroyImage($photo);

            if (!$isDeleteImageToServer) {
                return redirect()
                    ->route('admin_photo')
                    ->with('error', 'Photo cannot be deleted from server!');
            }

        } catch (\Exception $e) {
            Log::critical(sprintf('%s exception encountered when destroy a photo: "%s"', get_class($e), $e->getMessage()), ['exception' => $e]);
            return redirect()
                ->route('admin_photo')
                ->with('error', 'Error!');
        }

        return redirect()
            ->route('admin_photo')
            ->with('success', 'Photo has been deleted!');
    }
}
