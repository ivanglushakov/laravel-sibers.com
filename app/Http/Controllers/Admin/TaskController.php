<?php

namespace App\Http\Controllers\Admin;

use App\Events\TaskCreated;
use App\Helpers\TaskStatus;
use App\Http\Controllers\Controller;
use App\Jobs\SendReminderEmail;
use App\Repository\TaskCategoryRepository;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use App\Services\UploadImageService;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class TaskController extends Controller
{
    /**
     * @var UploadImageService
     */
    private $imageService;

    /**
     * @param UploadImageService $imageService
     */
    public function __construct(UploadImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * @param TaskRepository $taskRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(TaskRepository $taskRepository)
    {
        //App::setLocale('ru');

        return view('admin.task.list', [
            'tasks' => $taskRepository->all(),
        ]);
    }

    /**
     * @param Request $request
     * @param TaskCategoryRepository $categoryRepository
     * @param UserRepository $userRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request, TaskCategoryRepository $categoryRepository, UserRepository $userRepository)
    {
        if ($request->isMethod(Request::METHOD_POST)) {
            $this->validate($request, Task::rulesCreate(), Task::messages());

            /**@var Task $task*/
            $task = Task::create(array_merge(
                ['user_id' => Auth::id()],
                $request->all()
            ));

            $imageIds = $this->imageService->uploadImage($request, 'photo');

            if (!is_array($imageIds) && !$imageIds) {
                return redirect()
                    ->route('admin_task_create')->with('error', 'Error!');
            }

            $task->photo()->sync($imageIds);

            event(new TaskCreated($task));

            $when = Carbon::now()->addSeconds(5);
            //Auth::user()->notify((new TaskCreatedNotification($task))->delay($when));

            //$job = (new SendReminderEmail())->delay(Carbon::now()->addSeconds(10));
            $job = (new SendReminderEmail())->delay($when);

            dispatch($job);

            return back()->with('success','Task has created successfully!');
        }

        return view('admin.task.create', [
            'categories' => $categoryRepository->all(),
            'users'      => $userRepository->all(),
            'statuses'   => TaskStatus::getStatuses(),
        ]);
    }

    /**
     * @param TaskRepository $taskRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function assigmentMe(TaskRepository $taskRepository)
    {
        $userId = Auth::id();
        return view('admin.task.list', [
            'tasks' => $taskRepository->getByUserId($userId),
        ]);
    }

    /**
     * @param Task $task
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Task $task)
    {
        $this->authorize('view', $task);

        return view('admin.task.show', [
            'task' => $task,
            'photos' => $task->photo()->get(),
        ]);
    }

    /**
     * @param Task $task
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Task $task)
    {
        $this->authorize('delete', $task);

        $task->delete();

        return redirect()
            ->route('admin_task_assigmentMe')
            ->with('success', 'Task has been destroy!');
    }
}