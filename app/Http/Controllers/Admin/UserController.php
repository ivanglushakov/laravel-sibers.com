<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use App\User;
use Illuminate\Http\Request;
use Illuminate\View\View;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class UserController extends Controller
{
    /**
     * @param UserRepository $userRepository
     * @return View
     */
    public function index(UserRepository $userRepository)
    {
        return view('admin.user.list', [
            'users' => $userRepository->all()
        ]);
    }

    public function update(Request $request, User $user, RoleRepository $roleRepository)
    {
        $authUser = \Auth::user();
        $this->authorize('update', $authUser);

        if ($request->isMethod(Request::METHOD_POST)){
            $roles = $request->roles;

            $user->roles()->sync($roles);

            /**  TODO
             *   As mentioned in comments, if the model did not change the timestamps wont be updated.
             *   However if you need to update them, or want to check if everything is working fine use\
             *   $user->touch()
             */
            $user->touch();

            return redirect()->route('admin_user_update', [
                'user' => $user
            ])->with('success', 'Roles were assigned');
        }

        return view('admin.user.update', [
            'user'  => $user,
            'roles' => $roleRepository->all(),
            'checkedRolesNames' => $this->assignedRolesTheUser($user),
        ]);
    }

    /**
     * @param User $user
     * @return array
     */
    private function assignedRolesTheUser(User $user)
    {
        $checkedRolesNames = [];

        foreach ($user->roles()->get()->toArray() as $item) {

            if (!array_key_exists('name', $item)) {
                continue;
            }

            if (in_array($item['name'], $checkedRolesNames)) {
                continue;
            }

            $checkedRolesNames[] = $item['name'];
        }

        return $checkedRolesNames;
    }
}
