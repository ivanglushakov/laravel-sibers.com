<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function getUser()
    {
        return response([
            'success' => true,
            'user' => Auth::user(),
        ], 200);
    }
}