<?php

namespace App\Http\Controllers;

use App\Helpers\Generate;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class SocialController extends Controller
{
    /**
     * @param $provider
     * @return mixed
     */
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * @param $provider
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function callback($provider)
    {
        $userSocial = Socialite::driver($provider)->stateless()->user();
        $user       = User::where(['email' => $userSocial->getEmail()])->first();

        if ($user instanceof User) {
            return $this->loginUserWithRedirectToHomePage($user);
        } else {
            $user = User::create([
                'name'        => $userSocial->getName(),
                'email'       => $userSocial->getEmail(),
                'password'    => Hash::make(Generate::randomString()),
                'status'      => User::STATUS_ACTIVE,
                'provider_id' => $userSocial->getId(),
                'provider'    => $provider,
                'last_ip'     => \Request::ip(),
            ]);

            return $this->loginUserWithRedirectToHomePage($user);
        }
    }

    /**
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    private function loginUserWithRedirectToHomePage(User $user)
    {
        Auth::login($user);

        return redirect()->route('home');
    }
}
