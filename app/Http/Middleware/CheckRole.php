<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    public function handle(Request $request, Closure $next, $role)
    {
        $role = mb_strtoupper($role);
        /**@var User $user*/
        $user = Auth::user();

        if (!empty($user) && $role && !empty($user->roles)) {
            $roles = [];

            foreach ($user->roles as $role) {
                $roleName = $role->name;

                if (!in_array($roleName, $roles)) {
                    array_push($roles, $roleName);
                }
            }

            if (in_array($role, $roles)) {
                return $next($request);

            }
        }

        return abort(403);
    }
}
