<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class Photo extends Model
{
    protected $fillable = [
        'image',
        'thumbnail',
        'size_org',
        'size_org_compress',
        'width_org',
        'height_org',
        'size_thm',
        'size_thm_compress',
        'width_thm',
        'height_thm',
        'mime_type',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * The photos that belong to the tasks.
     */
    public function tasks()
    {
        return $this->belongsToMany(Task::class, 'tasks_photos');
    }
}
