<?php

namespace App\Listeners;

use App\Events\TaskCreated;
use App\Mail\SendEmailWhenTaskCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailTaskCreatedListener
{
    /**
     * @var boolean
     */
    private $isSendMail;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->isSendMail = env('MAIL_SEND');
    }

    /**
     * Handle the event.
     *
     * @param  TaskCreated  $event
     * @return void
     */
    public function handle(TaskCreated $event)
    {
        if ($this->isSendMail) {
            Mail::to(env('MAIL_USERNAME'))
                ->send(new SendEmailWhenTaskCreated($event->getTask()));
        }
    }
}
