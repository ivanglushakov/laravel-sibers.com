<?php

namespace App\Repository;

use App\TaskCategory;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class TaskCategoryRepository implements RepositoryInterface
{
   public function all()
   {
       return TaskCategory::all();
   }
}