<?php

namespace App\Repository;

use App\Task;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class TaskRepository implements RepositoryInterface
{
    private $paginate = 10;

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function all()
   {
       return Task::orderBy('created_at', 'DESC')
           ->orderBy('updated_at', 'DESC')
           ->paginate($this->paginate);
   }

    /**
     * @param $user_id
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getByUserId($user_id)
    {
        return Task::where('assigment_user_id', '=', $user_id)
            ->orderBy('status', 'ASC')
            ->orderBy('created_at', 'DESC')
            ->orderBy('updated_at', 'DESC')
            ->paginate($this->paginate);
    }
}