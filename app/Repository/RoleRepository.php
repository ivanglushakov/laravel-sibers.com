<?php

namespace App\Repository;

use App\Role;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class RoleRepository implements RepositoryInterface
{
    private $paginate = 5;

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function all()
    {
        return Role::orderBy('created_at', 'DESC')
            ->paginate($this->paginate);
    }
}