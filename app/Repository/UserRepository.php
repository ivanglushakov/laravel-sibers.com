<?php

namespace App\Repository;

use App\User;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class UserRepository implements RepositoryInterface
{
    private $paginate = 5;

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function all()
    {
        return User::orderBy('created_at', 'DESC')
            ->paginate($this->paginate);
    }
}