<?php

namespace App\Repository;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
interface RepositoryInterface
{
    public function all();
}