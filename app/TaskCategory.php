<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskCategory extends Model
{
    protected $table = 'tasks_category';

    protected $fillable = [
      'name'
    ];

    /**
     * Get tasks
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getTasks()
    {
        return $this->belongsTo(Task::class);
    }

}
