<?php

namespace App\Helpers;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class TaskStatus
{
    const HIGHEST = 'Highest';
    const HIGH    = 'High';
    const LOW     = 'Low';
    const LOWEST  = 'Lowest';

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            static::HIGHEST,
            static::HIGH,
            static::LOW,
            static::LOWEST,
        ];
    }
}