<?php

namespace App\Helpers;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class RoleUser
{
    const SUPER_ADMIN  = 'SUPER_ADMIN';
    const TASK_CREATE  = 'TASK_CREATE';
    const TASK_VIEW    = 'TASK_VIEW';
    const TASK_UPDATE  = 'TASK_UPDATE';
    const TASK_DESTROY = 'TASK_DESTROY';

    /**
     * Get array roles
     *
     * @return array
     */
    public static function getRoles()
    {
        return [
            static::SUPER_ADMIN,
            static::TASK_CREATE,
            static::TASK_VIEW,
            static::TASK_UPDATE,
            static::TASK_DESTROY,
        ];
    }
}