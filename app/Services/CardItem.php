<?php
declare(strict_types = 1);

namespace App\Services;

use App\Product;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class CardItem
{
    /**
     * @var Product
     */
    private $product;

    /**
     * @var int
     */
    private $quantity;

    /**
     * @param Product $product
     * @param int $quantity
     */
    public function __construct(Product $product, int $quantity = 1)
    {
        $this->product  = $product;
        $this->quantity = $quantity;
        $this->total    =  $this->calculate($quantity);
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Increment quality if card item already exist in card
     */
    public function incrementQuantity()
    {
        $this->quantity++;
    }

    /**
     * @param int $quantity
     * @return float
     */
    private function calculate(int $quantity):float
    {
        return round($this->product->amount * $quantity, 2);
    }
}