<?php
declare(strict_types = 1);

namespace App\Services;

use Stripe\Charge;
use Stripe\Stripe;
use Illuminate\Http\Request;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class StripePayment
{
    /**
     * @var string
     */
    private $currency;

    public function __construct()
    {
        Stripe::setApiKey(env('STRIPE_SECRET'));
        $this->currency = 'usd';
    }

    /**
     * @param Request $request
     * @param string  $userEmail
     * @return boolean
     */
    public function createCharge(Request $request, string $userEmail):bool
    {
        try {
            Charge::create([
                'currency'    => 'USD',
                "amount"      => $request->cost,
                "source"      => $request->stripeToken,
                "description" => "Charge for " . $userEmail,
            ]);

            return true;
        } catch (\Exception $e) {
            \Log::critical(sprintf('%s exception encountered when creating a charge: "%s"', get_class($e), $e->getMessage()), ['exception' => $e]);

            return false;
        }
    }
}