<?php
declare(strict_types = 1);

namespace App\Services;


/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class Card
{
    const SESSION_CARD = 'card';

    /**
     * @var array
     */
    private $cardItems = [];

    /**
     * @var float
     */
    private $total = 0.0;

    /**
     * @var int
     */
    private $countProduct = 0;

    /**
     * @param CardItem $item
     */
    public function addProduct(CardItem $item)
    {
        //dump($item);
        //dump($this->cardItems);

        //TODO Refactoring
        $neededObjectInCard = array_filter(
            $this->cardItems,
            function ($cardItemInCard) use (&$item) {
                return $cardItemInCard->getProduct()->id == $item->getProduct()->id;
            }
        );

        if (!empty($neededObjectInCard) && is_array($neededObjectInCard)) {
            foreach ($neededObjectInCard as $neededObject) {
                $neededObject->incrementQuantity();
            }
        } else {
            $this->cardItems[] = $item;
        }

        //dump($neededObject);

        $this->calculateTotal();
        $this->calculateCount();
    }

    /**
     * @return array
     */
    public function getItems():array
    {
        return $this->cardItems;
    }

    /**
     * Recalculate Card when add product in card
     */
    private function calculateTotal()
    {
        $total = 0;
        /**@var CardItem $cardItem */
        foreach ($this->cardItems as $cardItem) {
            $total += $cardItem->getProduct()->amount * $cardItem->getQuantity();
        }

        $this->total = $total;
    }

    /**
     * Calculate count in card item
     */
    private function calculateCount()
    {
        $count  = 0;
        /**@var CardItem $cardItem */
        foreach ($this->cardItems as $cardItem) {
            $count += $cardItem->getQuantity();
        }

        $this->countProduct = $count;
    }

    /**
     * @return int
     */
    public function getCountProduct(): int
    {
        return $this->countProduct;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return round($this->total, 2);
    }
}