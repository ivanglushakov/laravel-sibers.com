<?php
declare(strict_types = 1);

namespace App\Services;

use File;
use Image;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
final class UploadImageService
{
    /**
     * @var string
     */
    private $pathOriginal;

    /**
     * @var string
     */
    private $pathThumbnails;

    /**
     * UploadImageService constructor.
     */
    public function __construct()
    {
        $this->pathOriginal   = env('PATH_IMAGE_ORIGINALS');
        $this->pathThumbnails = env('PATH_IMAGE_THUMBNAILS');
    }

    /**
     * @param Request $request
     * @param string $fieldImage
     * @return array|bool
     */
    public function uploadImage(Request $request, string $fieldImage)
    {
        $savedModelIds = [];

        if (!$request->hasFile($fieldImage)) {
            return false;
        }

        $images = $request->file($fieldImage);

        //setting flag for condition
        $org_img = $thm_img = true;

        // create new directory for uploading image if doesn't exist
        if( ! File::exists($this->pathOriginal)) {
            $org_img = File::makeDirectory($this->pathOriginal, 0777, true);
        }

        if ( ! File::exists($this->pathThumbnails)) {
            $thm_img = File::makeDirectory($this->pathThumbnails, 0777, true);
        }

        /**@var UploadedFile $image*/
        foreach($images as $key => $image) {
            /**@var Photo $newPhoto*/
            $newPhoto = new Photo();

            $filename = rand(1111,9999).time().'.'.$image->getClientOriginalExtension();
            $org_path = $this->pathOriginal . $filename;
            $thm_path = $this->pathThumbnails . $filename;

            $newPhoto->image     = $this->pathOriginal.$filename;
            $newPhoto->thumbnail = $this->pathThumbnails.$filename;
            $newPhoto->mime_type = $image->getMimeType();
            $newPhoto->size_org  = $image->getSize();

            if (($org_img && $thm_img) == true) {
                $imageOrg = $this->uploadImageToServer($image, 900, 500, $org_path);

                $newPhoto->size_org_compress = $imageOrg->filesize();
                $newPhoto->width_org  = $imageOrg->getWidth();
                $newPhoto->height_org = $imageOrg->getHeight();

                $imageThm = $this->uploadImageToServer($image, 270, 160, $thm_path);

                $newPhoto->size_thm_compress = $imageThm->filesize();
                $newPhoto->width_thm  = $imageThm->getWidth();
                $newPhoto->height_thm = $imageThm->getHeight();
            }

            if ( !$newPhoto->save()) {
                return false;
            }

            if (!in_array($newPhoto->id, $savedModelIds)) {
                $savedModelIds[] = $newPhoto->id;
            }
        }

        return $savedModelIds;
    }

    /**
     * @param UploadedFile $image
     * @param int $width
     * @param int $height
     * @param string $orgPath
     * @return \Intervention\Image\Image
     */
    private function uploadImageToServer(UploadedFile $image, int $width, int $height, string $orgPath):\Intervention\Image\Image
    {
        $image = Image::make($image)->fit($width, $height, function ($constraint) {
            $constraint->upsize();
        });

        if (env('WATERMARK_ADD')) {
            $image->insert(
                public_path(env('WATERMARK_IMAGE_PATH')),
                env('WATERMARK_IMAGE_POSITION'),
                env('WATERMARK_IMAGE_POSITION_X'),
                env('WATERMARK_IMAGE_POSITION_Y')
            );
        }

        return $image->save($orgPath);
    }

    /**
     * @param Photo $photo
     * @return bool
     */
    public function destroyImage(Photo $photo)
    {
        $org_path = $photo->image;
        $thm_path = $photo->thumbnail;

        if (!File::exists($org_path) && !File::exists($thm_path)) {
            return false;
        }

        File::delete($org_path);
        File::delete($thm_path);

        return true;
    }
}