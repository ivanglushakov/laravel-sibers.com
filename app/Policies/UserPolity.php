<?php

namespace App\Policies;

use App\Helpers\RoleUser;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolity
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function update(User $user)
    {
        return $user->hasRole(RoleUser::SUPER_ADMIN);
    }
}
