<?php

namespace App\Policies;

use App\Helpers\RoleUser;
use App\User;
use App\Task;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function view(User $user, Task $task)
    {
        return $this->isUserAssigmentTask($user, $task) ||
            $user->hasRole(RoleUser::SUPER_ADMIN) ||
            $user->hasRole(RoleUser::TASK_VIEW);
    }

    /**
     * Determine whether the user can create tasks.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole(RoleUser::SUPER_ADMIN) || $user->hasRole(RoleUser::TASK_CREATE);
    }

    /**
     * Determine whether the user can update the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function update(User $user, Task $task)
    {
        return $user->hasRole(RoleUser::SUPER_ADMIN) || $user->hasRole(RoleUser::TASK_UPDATE);
    }

    /**
     * Determine whether the user can delete the task.
     *
     * @param  \App\User  $user
     * @param  \App\Task  $task
     * @return mixed
     */
    public function delete(User $user, Task $task)
    {
        return $this->isOwnerTask($user, $task) || $user->hasRole(RoleUser::SUPER_ADMIN);
    }

    /**
     * Checks if a user is owner task
     *
     * @param User $user
     * @param Task $task
     * @return bool
     */
    private function isOwnerTask(User $user, Task $task)
    {
        return $user->id === $task->user_id;
    }

    /**
     * Checks if a user is assigment task
     *
     * @param User $user
     * @param Task $task
     * @return bool
     */
    private function isUserAssigmentTask(User $user, Task $task)
    {
        return $user->id === $task->assigment_user_id;
    }
}
