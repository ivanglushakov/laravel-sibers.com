<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class Role extends Model
{
    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user');
    }
}
