<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])
    ->prefix('admin')
        ->group(function () {
            Route::get('home', 'HomeController@index')->name('home');
            Route::get('user/{user}', 'Admin\UserController@index')->name('admin_user_show');

            Route::prefix('task')->group(function () {
                Route::match(['get', 'post'],'create', 'Admin\TaskController@create')->name('admin_task_create');
                Route::match(['get'],'list', 'Admin\TaskController@index')->name('admin_task_list');
                Route::match(['get'],'show/{task}', 'Admin\TaskController@show')->name('admin_task_show');
                Route::delete('destroy/{task}', 'Admin\TaskController@destroy')->name('admin_task_destroy');

                Route::match(['get'],'me', 'Admin\TaskController@assigmentMe')->name('admin_task_assigmentMe');
            });

            Route::prefix('user')->group(function () {
                Route::match(['get'],'list', 'Admin\UserController@index')->name('admin_user_list');
                Route::match(['get', 'post'],'{user}/update', 'Admin\UserController@update')->name('admin_user_update');
            });

            Route::get('payment', 'Admin\PayController@payment')->name('admin_payment');
            Route::post('charge', 'Admin\PayController@createCharge')->name('admin_charge');

            Route::prefix('photo')->group(function () {
                Route::get('/', 'Admin\PhotoController@index')->name('admin_photo');
                Route::post('/', 'Admin\PhotoController@uploadImage')->name('admin_photo_upload');
                Route::post('{photo}/destroy', 'Admin\PhotoController@destroy')->name('admin_photo_destroy');
            });
});

Route::middleware(['auth'])
    ->prefix('shop')
        ->group(function () {
            Route::get('/', 'ShopController@productList')->name('shop_product_list');
            Route::post('/add', 'ShopController@addProductInCard')->name('shop_add_product_in_card');
        });

Route::middleware(['auth'])
    ->prefix('card')
    ->group(function () {
        Route::get('/', 'CardController@index')->name('card_index');
    });


Route::middleware(['guest'])->group(function () {
    Route::get('login/{provider}', 'SocialController@redirect')->name('social_login');
    Route::get('login/{provider}/callback','SocialController@callback')->name('social_login_callback');

    Route::get('/verify/{token}', 'Auth\RegisterController@verify')->name('register.verify');
});
