<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Public Route
Route::post('/login','Api\AuthenticationController@login')->name('api_login');

// Private Route
Route::middleware('auth:api')->group(function () {
    Route::get('/logout','Api\AuthenticationController@logout')->name('api_logout');

    Route::get('/user','Api\UserController@getUser')->name('api_me');
});