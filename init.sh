php artisan key:generate
#php artisan migrate:refresh
#php artisan migrate:refresh --seed
php artisan migrate:fresh --seed

composer dump-autoload
php artisan db:seed

php artisan passport:keys --force
php artisan passport:install --force