<?php

use App\Helpers\RoleUser;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

/**
 * Class RoleSeeder
 */
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (RoleUser::getRoles() as $item) {
            $role = Role::create([
                'name' => $item,
            ]);

            echo "Role = ". $role->name . " has been created!\n";
        }

        $userIvan       = User::where('email','ivan.glushakov@sibers.com')->first();
        $roleSuperAdmin = Role::where('name','SUPER_ADMIN')->first();

        if ($userIvan instanceof User && $roleSuperAdmin instanceof Role) {
            $userIvan->roles()->attach($roleSuperAdmin->id);
        }
    }
}
