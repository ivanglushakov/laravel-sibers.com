<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = [1, 2, 3, 4, 5];

        foreach ($ids as $id) {
            Product::create([
                'name'   => "Product $id",
                'amount' => rand(100, 1000),
            ]);
        }
    }
}
