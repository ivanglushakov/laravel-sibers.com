<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = [2,3,4,5,7];

        $userIvan = User::create([
            'name' => "Ivan Glushakov",
            'email' => "ivan.glushakov@sibers.com",
            'password' => Hash::make("qwerty"),
            'status' => User::STATUS_ACTIVE
        ]);

        echo "User ". $userIvan->email . "has been created!";

        foreach ($ids as $id) {
            $user = User::create([
                'name' => "Test$id",
                'email' => "test$id@sibers.com",
                'password' => Hash::make("test$id"),
                'status' => User::STATUS_ACTIVE
            ]);

            echo "User ". $user->email . "has been created!";
        }
    }
}
