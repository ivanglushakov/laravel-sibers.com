<?php

use App\TaskCategory;
use Illuminate\Database\Seeder;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class TaskCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ids = [1,2,3,4,5];

        foreach ($ids as $id) {
            TaskCategory::create([
                'name' => "Category_$id",
            ]);
        }
    }
}
