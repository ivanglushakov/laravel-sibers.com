<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class AddFkTasksTableViaTasksPhotosWithPhotosTable
 */
class AddFkTasksTableViaTasksPhotosWithPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks_photos', function (Blueprint $table) {
            $table->unsignedBigInteger('task_id');
            $table->unsignedBigInteger('photo_id');

            $table->foreign('task_id')->references('id')->on('tasks')
                ->onDelete('cascade');

            $table->foreign('photo_id')->references('id')->on('photos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tatasks_photossks_photos');
    }
}
