<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image');
            $table->string('thumbnail');
            $table->bigInteger('size_org');
            $table->bigInteger('size_org_compress');
            $table->bigInteger('width_org');
            $table->bigInteger('height_org');
            $table->bigInteger('size_thm_compress');
            $table->bigInteger('width_thm');
            $table->bigInteger('height_thm');
            $table->string('mime_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
