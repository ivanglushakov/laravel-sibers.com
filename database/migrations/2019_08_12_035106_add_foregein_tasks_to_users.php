<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForegeinTasksToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table){
            $table->foreign('user_id', 'fk-task-user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('assigment_user_id', 'fk-task-assigment_user')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table){
            $table->dropForeign('fk-task-assigment_user');
            $table->dropForeign('fk-task-user');
        });
    }
}
