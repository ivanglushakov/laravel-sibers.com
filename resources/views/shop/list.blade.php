@extends('layouts.app')

@section('container-title')
    Shop
@endsection

@section('content')
    @if (!empty($products))
        @include('layouts.header-shop', ['count' => $count, 'total' => $total])
        <table class="table table-product">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th colspan="2">Amount</th>
            </tr>
            </thead>
            <tbody>
            @each('shop.itemProduct', $products, 'product')
            </tbody>
        </table>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <a href="{{route('card_index')}}" class="btn btn-success">Card</a>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('javascripts')
    @parent
    <script>
        $( document ).ready(function() {

            let tableProduct = $(".table-product");

            if (tableProduct.length < 0) {
                return false;
            }

            tableProduct.find(".add-product").each(function (element) {
                let $link = $(this);

                $link.on("click", function () {
                    let $productId = $link.data('product');

                    $.ajax({
                        url: "{{route('shop_add_product_in_card')}}",
                        type: "POST",
                        data:{
                            _token: "{{ csrf_token() }}",
                            product: $productId, // Second add quotes on the value.
                        },
                        success: function( resp ) {
                            $("span.card-items-count").html(resp.count);
                            $("span.card-total").html(resp.total);
                        },
                    });
                });
            });
        });
    </script>

@endsection
