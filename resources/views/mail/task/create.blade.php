@component('mail::message')
# Task #{{$task->id}} created

@component('mail::button', ['url' => route('admin_task_show', ['task' => $task])])
Show task
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
