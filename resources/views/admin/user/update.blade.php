@extends('layouts.app')

@section('container-title')
    User Update
@endsection

@section('content')
    @can('update', \App\User::class)
        <!-- The Current User Can Create Posts -->
        <form class="form-horizontal" method="POST" action="{{ route('admin_user_update', ['user' => $user]) }}" novalidate>
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('assigment_user_id') ? ' has-error' : '' }}">
                <label for="roles" class="col-md-4 control-label">Roles</label>

                <div class="col-md-6">
                    <select id="roles" name="roles[]" class="form-control" multiple="multiple">
                        @forelse($roles as $role)
                            <option value="{{$role->id}}"
                                @if (in_array($role->name, $checkedRolesNames))
                                    selected="selected"
                                @endif
                            >
                                {{$role->name}}
                            </option>
                        @empty
                            Role is empty!
                        @endforelse
                    </select>

                    @if ($errors->has('roles'))
                        <span class="help-block">
                        <strong>{{ $errors->first('roles') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Save
                    </button>
                </div>
            </div>
        </form>
    @endcan

    @cannot('update', App\User::class)
        <!-- The Current User Can't Create Posts -->
        <p>You are denied access to edit a user. <br>
            Please contact administrator</p>
    @endcannot
@endsection

@section('javascripts')
    @parent
    <script>
        $(document).ready(function() {
            $('#roles').select2();
        });
    </script>
@endsection
