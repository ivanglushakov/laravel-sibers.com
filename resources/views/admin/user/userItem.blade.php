<tr>
    <td>{{$user->id}}</td>
    <td>{{$user->name}}</td>
    <td>{{$user->email}}</td>
    <td>{{$user->created_at}}</td>
    <td>{{$user->updated_at}}</td>
    <td>
        <a href="{{route('admin_user_update', ['user' => $user])}}" class="btn btn-sm btn-info">Edit</a>
    </td>
    <td>
        {{--<form action="{{route('admin_task_destroy', ['task' => $user->id])}}" method="post">--}}
            {{--{{ csrf_field() }}--}}
            {{--{{ method_field('delete') }}--}}
            {{--<button type="submit" class="btn btn-sm btn-danger">Delete</button>--}}
        {{--</form>--}}
    </td>
</tr>