@extends('layouts.app')

@section('container-title')
    User List
@endsection

@section('content')
    @if (!empty($users))
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th colspan="2">Actions</th>
            </tr>
            </thead>
            <tbody>
            @each('admin.user.userItem', $users, 'user')
            </tbody>

        </table>
        <div class="pagination">
            {{$users->links()}}
            {{--{{ $users->appends(['sort' => 'created_at'])->links() }}--}}
        </div>
    @endif
@endsection
