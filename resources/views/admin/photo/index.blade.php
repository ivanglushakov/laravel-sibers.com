@extends('layouts.app')

@section('container-title')
    Upload Multiple Images
@endsection

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            Please correct following errors:
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <input type="file" name="image[]" class="form-control-file" multiple="true">
        </div>

        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <hr>
    <h3>Listing Images</h3>

    @forelse($photos as $photo)
        <div class="col-md-3">
            <img src="{{ asset($photo->thumbnail) }}" class="img-responsive" alt="#">
            <form action="{{route('admin_photo_destroy', ['photo' => $photo])}}" method="post">
                <input type="hidden" name="image" value="{{$photo->id}}">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-sm btn-danger">Del</button>
            </form>

        </div>
    @empty
        No image found
    @endforelse
@endsection