@extends('layouts.app')

@section('container-title')
    Create Task
@endsection

@section('content')
    @can('create', \App\Task::class)
        <!-- The Current User Can Create Posts -->
        <form class="form-horizontal" method="POST" action="{{ route('admin_task_create') }}" enctype="multipart/form-data" novalidate >
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('assigment_user_id') ? ' has-error' : '' }}">
                <label for="assigment" class="col-md-4 control-label">Assigment User</label>

                <div class="col-md-6">
                    <select id="assigment" name="assigment_user_id" class="form-control">
                        @forelse($users as $user)
                            <option value="{{$user->id}}">{{$user->name}}</option>
                        @empty
                            User is empty!
                        @endforelse
                    </select>

                    @if ($errors->has('assigment_user_id'))
                        <span class="help-block">
                        <strong>{{ $errors->first('assigment_user_id') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                <label for="category" class="col-md-4 control-label">Category</label>

                <div class="col-md-6">
                    <select id="category" name="category_id" class="form-control">
                        @forelse($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @empty
                            Categories is empty!
                        @endforelse
                    </select>

                    @if ($errors->has('category_id'))
                        <span class="help-block">
                        <strong>{{ $errors->first('category_id') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="col-md-4 control-label">Name</label>

                <div class="col-md-6">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                <label for="photo" class="col-md-4 control-label">Photo</label>

                <div class="col-md-6">
                    <input type="file" name="photo[]" class="form-control-file" multiple="true">

                    @if ($errors->has('photo'))
                        <span class="help-block">
                        <strong>{{ $errors->first('photo') }}</strong>
                    </span>
                    @endif

                </div>
            </div>

            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                <label for="status" class="col-md-4 control-label">Status</label>

                <div class="col-md-6">
                    <select id="status" name="status" class="form-control">
                        @forelse($statuses as $status)
                            <option value="{{$status}}">{{$status}}</option>
                        @empty
                            Status is empty!
                        @endforelse
                    </select>

                    @if ($errors->has('status'))
                        <span class="help-block">
                        <strong>{{ $errors->first('status') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                <label for="description" class="col-md-4 control-label">Description</label>

                <div class="col-md-6">
                    <textarea name="description" class="form-control my-editor" ></textarea>

                    @if ($errors->has('description'))
                        <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Create
                    </button>
                </div>
            </div>
        </form>
    @endcan

    @cannot('create', App\Task::class)
        <!-- The Current User Can't Create Posts -->
        <p>You are denied access to create a task. <br>
            Please contact administrator</p>
    @endcannot
@endsection

@section('javascripts')
    @parent
    <script>
        $(document).ready(function() {
            $('#assigment').select2();
        });
    </script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea.my-editor",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>
@endsection

