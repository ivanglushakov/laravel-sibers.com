<tr>
    <td>{{$task->id}}</td>
    <td>{{$task->name}}</td>
    <td>{{$task->status}}</td>
    <td>{{$task->created_at}}</td>
    <td>{{$task->updated_at}}</td>
    <td>
        <a href="{{route('admin_task_show', ['task' => $task->id])}}" class="btn btn-sm btn-info">Show</a>
    </td>
    <td>
        <form action="{{route('admin_task_destroy', ['task' => $task->id])}}" method="post">
            {{ csrf_field() }}
            {{ method_field('delete') }}
            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
        </form>
    </td>
</tr>