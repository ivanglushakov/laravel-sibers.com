@extends('layouts.app')

@section('container-title')
    Show Task {{$task->name}}
@endsection

@section('content')
    <p>Name: {{$task->name}}</p>
    <p>Category: {{$task->category->name}}</p>
    <p>Status: {!! $task->status !!}</p>
    <p>Description: {!! $task->description !!}</p>

    <h3>Listing Images</h3>

    @forelse($photos as $photo)
        <div class="col-md-3">
            <img src="{{ asset($photo->thumbnail) }}" class="img-responsive" alt="#">
        </div>
    @empty
        No image found
    @endforelse
@endsection
