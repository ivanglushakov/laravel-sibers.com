@extends('layouts.app')

@section('container-title')
    Task List
@endsection

@section('content')
    @lang('messages.welcome', ['Name' => 'Ivan'])
    @if (!empty($tasks))
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>Task Name</th>
                <th>Status</th>
                <th>Created At</th>
                <th>Updated At</th>
                <th colspan="2">Actions</th>
            </tr>
            </thead>
            <tbody>
            @each('admin.task.taskItem', $tasks, 'task')
            </tbody>

        </table>
        <div class="pagination">
            {{$tasks->links()}}
        </div>
    @endif
@endsection
