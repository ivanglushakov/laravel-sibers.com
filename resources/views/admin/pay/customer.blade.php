@extends('layouts.app')

@section('container-title')
    Customer
@endsection

@section('content')
    <div class="customer">
        Customer: {{$customer->email}}
        <p>Balance: {{$customer->balance}}</p>
    </div>

    <div class="payment">
        <a href="{{route('admin_payment')}}">Payment</a>
    </div>
@endsection