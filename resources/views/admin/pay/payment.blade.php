@extends('layouts.app')

@section('container-title')
    Payment
@endsection

@section('stylesheet')
    @parent
    <link href="{{ asset('css/stripe.css') }}" rel="stylesheet">
@endsection

@section('content')
    <form action="{{route('admin_charge')}}" method="post" id="payment-form">
        @csrf
        <div class="form-group">
            <div class="card-header">
                <label for="card-element">
                    Enter your credit card information
                </label>
            </div>
            <div class="card-body">
                <div id="card-element">
                    <!-- A Stripe Element will be inserted here. -->
                </div>
                <!-- Used to display form errors. -->
                <div id="card-errors" role="alert"></div>
            </div>
        </div>

        <div class="form-group">
            <div class="cost-header">
                <label for="cost">
                    Enter your cost
                </label>
            </div>

            <div class="cost-body">
                <input id="cost" type="text" class="form-control" name="cost">
            </div>
        </div>

        <div class="card-footer">
            <button class="btn btn-dark" type="submit">Pay</button>
        </div>
    </form>
@endsection

@section('javascripts')
    @parent
    <script>
        var stripe = Stripe("{{ env("STRIPE_KEY") }}");

        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();

            let userName  = "{{ Auth::user()->name }}";
            let userEmail = "{{ Auth::user()->email }}";

            let data = {
                name: userName ? userName : undefined,
                email: userEmail ? userEmail : undefined,
            };

            stripe.createToken(card, data).then(function(result) {
                if (result.error) {
                    // Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });

        // Submit the form with the token ID.
        function stripeTokenHandler(token) {
            // Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);

            // Submit the form
            form.submit();
        }
    </script>
@endsection