<div class="row">
    <div class="col-md-4">
        <div class="social-link">
            <p>Login with</p>
            <a href="{{ route('social_login', ['provider' => 'google']) }}" class="btn btn-google-plus"> Google</a>
            <a href="{{ route('social_login', ['provider' => 'github']) }}" class="btn btn-github"> Github</a>
        </div>
    </div>
</div>
