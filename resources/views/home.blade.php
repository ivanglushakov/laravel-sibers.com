@extends('layouts.app')

@section('container-title')
    Dashboard
@endsection

@section('stylesheet')
    @parent
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
@endsection

@section('content')
    You are logged in!

    <div id="myfirstchart" style="height: 250px;"></div>

@endsection

@section('javascripts')
    @parent
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script>
        new Morris.Bar({
            // ID of the element in which to draw the chart.
            element: 'myfirstchart',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                @foreach($photos as $key => $photo)
                { day: "{{$key}}", value: "{{$photo}}" },
                @endforeach
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value'],
            barRatio: 0.4,
            xLabelAngle: 35,
            hideHover: 'auto',
        });
    </script>
@endsection


