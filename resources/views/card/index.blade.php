@extends('layouts.app')

@section('container-title')
    Card
@endsection

@section('content')
    @include('layouts.header-shop', ['count' => $count, 'total' => $total])
    @if (!empty($card))
        <table class="table table-product">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th colspan="2">Amount</th>
            </tr>
            </thead>
            <tbody>
            @each('card.item', $items, 'item')
            </tbody>
        </table>
    @endif
@endsection

