<div class="row">
    <div class="col-md-offset-6 col-md-6 text-right">
        <div class="row">
            <div class="col-md-9">
                <div class="card">
                    <i class="fa fa-shopping-basket"></i>
                    <span class="card-items-count">{{$count}}</span>
                    <span class="card-items">items</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="total">
                    <i class="fa fa-money"></i>
                    <span class="card-total">{{$total}}</span>
                    <span class="card-currency">USD</span>
                </div>
            </div>
        </div>
    </div>
</div>