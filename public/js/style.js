/**
 * @author Ivan Glushakov <ivan.glushakov@sibers.com>
 */
"use strict";

$( document ).ready(function () {

   function Calculator() {
      this.read = function() {
         let value1 = prompt("Val 1 = ", "3");
         let value2 = prompt("Val 2 = ", "2");

         if (value1 === "" && value2 === "") {
            alert("Error!");
            return false;
         }

         this.value1 = value1;
         this.value2 = value2;
      };

      this.sum = function () {
         return (+this.value1 + +this.value2);
      };

      this.mul = function () {
         return (+this.value1 * +this.value2);
      };
   }

   let calculator = new Calculator();
   calculator.read();
   alert( "Sum=" + calculator.sum() );
   alert( "Mul=" + calculator.mul() );

});